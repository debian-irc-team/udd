# Ultimate Debian Database (UDD)

A collection of data about Debian to support work on QA (Quality Assurance), in
SQL format.


## History

The UDD started in 2008 as a Google Summer of Code project. The student (who
did most of the coding), was Christian von Essen. The mentor was Lucas
Nussbaum, and the co-mentors were Stefano Zacchiroli and Marc Brockschmidt.


## Data Sources

The imported data comes from different sources. Each source has a specific type
(e.g. `popcon`). For each such type, there is a program to import this data into
the database (a "gatherer"). Also, there is an optional way to update the data
(i.e. get it from the source) for each source.

Each source has its own documentation. See `doc/sources/`.


## Local Setup

### 1. Download

- Get the code from https://salsa.debian.org/qa/udd/


    # clone over https:
    git clone https://salsa.debian.org/qa/udd.git

    # If you register in salsa, you can also clone it over ssh:
    git clone git@salsa.debian.org:qa/udd.git


### 2. Set up a Development Environment

#### Option 1 (recommended): Vagrant

Probably, the easiest way to hack on the UDD is using the Vagrant development
environment.  Just run `vagrant up` in the UDD source code. See `Vagrantfile`
and the scripts in `vagrant/` for details.

This development environment supports setting up tunnels to access the main
instance remotely, or dumping/importing data locally, depending on what you
want to work on.

#### Option 2: Manually

If you don't want to (learn how to) use Vagrant, your best bet is to look at the Vagrant
provision scripts in `vagrant/` to understand how to setup your own instance manually.


### 3. Configuration

- Edit `<your-config-file.yaml>` (see `doc/README.config` for details)
- Setup the DB: `psql udd < sql/udd-schema.sql`
- Initialize the DB:
      for i in \
        $(cat <your-config-file.yaml> | grep -v "^ " | grep ":" | grep -v general|sed 's/://')
            do <path/to/update-and-run.sh> $i
      done

### 4. Execution

- Fetch external data: `./udd.py <configuration> update`
- Import the data into the DB: `./udd.py <your-config-file.yaml> run`

## Troubleshooting

In case a transaction is waiting in idle mode you should do the following:

 1. Kill all idle transactions:


      psql udd -c "SELECT * FROM dsa_kill_all_idle_transactions();"


 2. Kill running importers


## Licensing

According to their nature, different elements from this project are covered by
one of two licenses.

The script code used to import data and/or generate the database, together with
their related code, are both licensed under the GNU GPL3+.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    See LICENSE.GPLv3 for a local copy of the GNU General Public License. If not, see <https://www.gnu.org/licenses/>.


The database itself, and its related data are both licensed under the Open Data
Commons Open Database License (ODbL) v1.0.

    This database is made available under the Open Database License:
    http://opendatacommons.org/licenses/odbl/1.0/. The contents comes from
    many places in the Debian ecosystem, and may be covered by its own
    license.

    See LICENSE.ODbLv1 for a local copy of the license.
