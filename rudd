#!/usr/bin/ruby

$:.unshift File.dirname(__FILE__) + '/rlibs'
$:.unshift File.dirname(__FILE__) + '/rimporters'

require 'udd-db'
require 'peach'
require 'pg'
require 'pp'
require 'json'
ENV['SSL_CERT_FILE'] = '/etc/ssl/ca-global/ca-certificates.crt'
require 'openssl'
require 'open-uri'
require 'fileutils'
require 'rexml/document'
require 'open3'
require 'udd-utils'
require 'udd-daemon'

require 'ci'
require 'security-tracker'
require 'upstream'
require 'duck'
require 'sanity-checks'
require 'wanna-build'
require 'migration_excuses'
require 'optparse'

OptionParser::new do |opts|
  opts.on('--daemon', 'Start the daemon') do
    run_daemon
    exit(0)
  end
  opts.on('--html-status', 'Show status (html version, for web version)') do |imp|
    udd_status_html
    exit(0)
  end
  opts.on('--status', 'Show status (text version, for cron emails)') do |imp|
    udd_status
    exit(0)
  end
  opts.on('--run IMPORTER', 'Run importer') do |imp|
    imp = load_importers[imp]
    res = run_importer(imp)
    exit(res)
  end
  opts.on('--built-in TASK', 'Execute built-in task (mostly useful for debugging)') do |task|
    case task
    when 'ci' ; update_ci
    when 'security-tracker' ; update_security_tracker
    when 'upstream' ; Upstream::update_upstream
    when 'duck' ; update_duck
    when 'sanity-checks' ; update_sanity_checks
    when 'wanna-build' ; update_wanna_build
    when 'migration-excuses' ; update_migration_excuses
    else
      puts "Invalid task: #{task}"
      exit(1)
    end
    exit(0)
  end
end.parse!(ARGV)

raise 'No option specified'
