Check whether a vacuum is working:

SELECT relname, reltuples, relpages
FROM pg_class
ORDER BY relpages DESC;

;; -(thanks Q_)


DB size:

SELECT pg_size_pretty(pg_database_size('udd'));


Relations size:

SELECT relname, pg_total_relation_size(relname::text) size, pg_size_pretty(pg_total_relation_size(relname::text))
FROM pg_class
WHERE relnamespace=2200
AND relkind='r'
ORDER BY size DESC;
