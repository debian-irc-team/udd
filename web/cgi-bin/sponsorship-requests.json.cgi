#!/usr/bin/ruby

Encoding.default_internal='UTF-8'

$:.unshift('../../rlibs')
require 'udd-db'
require 'pp'
require 'uri'
require 'net/http'
require 'json/pure'
require 'pp'

puts "Content-type: application/json\n\n"

DB = Sequel.connect(UDD_GUEST)

rows = DB["select * from bugs where package='sponsorship-requests'"].all.sym2str

ids = rows.map { |r| r['id'] }

rowst = DB["select * from bugs_tags where id in (#{ids.join(',')})"].all.sym2str

tags = {}
rowst.each do |r|
  tags[r['id']] ||= []
  tags[r['id']] << r['tag']
end

h = []

rows.each do |r|
  mybug = r.to_h
  if tags[mybug['id']]
    mybug['tags'] = tags[mybug['id']]
  else
    mybug['tags'] = []
  end
  h << mybug
end

puts JSON::pretty_generate(h)
